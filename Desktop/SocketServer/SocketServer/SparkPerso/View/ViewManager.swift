//
//  SocketManager.swift
//  SparkPerso
//
//  Created by digital on 23/01/2019.
//  Copyright © 2019 ElodieOudot. All rights reserved.
//

import UIKit
import VideoPreviewer

class ViewManager {
    static let shared = ViewManager()
    
    var ticker: UILabel?
    var videoView: UIView?
    let prev1 = VideoPreviewer()

    func initLabel(label: UILabel, video: UIView){
        ticker = label
        videoView = video
    }
    
    func startVideo() {
        prev1?.setView(self.videoView)
        prev1?.start()
    }
    
    func feedVideoView(data : Data){
        
            data.withUnsafeBytes{(bytes: UnsafePointer<UInt8>) in
                print("update video preview")
                prev1?.push(UnsafeMutablePointer(mutating: bytes), length: Int32(data.count))
            }
        
    }
    
    
    
    
}
