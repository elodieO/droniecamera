//
//  SocketData.swift
//  SparkPerso
//
//  Created by digital on 24/01/2019.
//  Copyright © 2019 AlbanPerli. All rights reserved.
//

import SocketIO

struct SendMessage : SocketData {
    let message: String
    
    func socketRepresentation() -> SocketData {
        return ["message": message]
    }
}
