//
//  SocketManager.swift
//  SparkPerso
//
//  Created by digital on 23/01/2019.
//  Copyright © 2019 ElodieOudot. All rights reserved.
//

import SocketIO

class SocketIOManager :NSObject {
    
    static let shared = SocketIOManager()
    
    var socket: SocketIOClient!
    var msgNumber: Int = 0
    
    // defaultNamespaceSocket and swiftSocket both share a single connection to the server
    let manager = SocketManager(socketURL: URL(string: "https://floating-atoll-16267.herokuapp.com/")!, config: [.log(false), .compress, .forcePolling(true), .forceNew(true)])
    
    override init() {
        super.init()
        socket = manager.defaultSocket
    }
    
    func connectSocket() {
        print("Socket connect")
        socket.on("connect") { _, _ in
            print("socket connected")
            
            self.socket.on("time") { (dataArray, ack) in
                print("received time event")
                self.msgNumber += 1
                ViewManager.shared.ticker?.text = String(self.msgNumber)
                
            }
            self.socket.on("monMessage") { (dataArray, ack) in
                print("received monMessage event")
            }
            
            self.socket.on("toVideo") { (dataArray, ack) in

                print("video infos !")
                // Ici je récupère bien des bytes à l'identique de ce que m'envoie Jonathan
                print(dataArray)

                // J'envoie dans le ViewManager les données
                // Le format attendu par feedVideoView est de type Data
                ViewManager.shared.feedVideoView(data: dataArray)

            }
            
        }
        socket.connect()
        
       
        
    }
    
    func receiveMsg() -> Data{
        
    }
    
    func sendMsg() {
        print("send msg")
        self.socket.emit("test", "Hello")
    }
    
    
}
