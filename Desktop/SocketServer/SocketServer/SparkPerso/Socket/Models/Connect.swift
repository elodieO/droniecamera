//
//  Connect.swift
//  Djadja
//
//  Created by Digital on 07/12/2018.
//  Copyright © 2018 Digital. All rights reserved.
//


//   let connect = try? newJSONDecoder().decode(Connect.self, from: jsonData)


import Foundation

struct Connect: Codable {
    let event: String
    let data: DataClassConnect
}

struct DataClassConnect: Codable {
    let pseudo: String
}
