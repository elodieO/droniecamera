//
//  ViewController.swift
//  SparkPerso
//
//  Created by AL on 14/01/2018.
//  Copyright © 2018 AlbanPerli. All rights reserved.
//

import UIKit
import SocketIO
import DJISDK

class ConnectionViewController: UIViewController {
    
    @IBOutlet weak var ticker: UILabel!
    var tickerValue:Int = 0

    @IBOutlet weak var videoPreview: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        ViewManager.shared.initLabel(label: ticker, video: videoPreview)
        SocketIOManager.shared.connectSocket()
        ViewManager.shared.startVideo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendButtonClicked(_ sender: Any) {
        SocketIOManager.shared.sendMsg()
    }
}
